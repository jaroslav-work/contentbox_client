<?php
/**
 * Created by PhpStorm.
 * User: jaroslaw
 * Date: 20.06.17
 * Time: 15:55
 */

return [
        'catalog' => [
            'attr' => [
                'date' => ''
            ],

            'categories' => [
                'category' => [
                    'attr' => [
                        'id' => 0,
                        'parent_id' => 0
                    ]
                ],

            ],

            'brands' => [
                'brand' => [
                    'attr' => [
                        'id' => 0,
                    ]
                ]
            ],

            'attributes' => [
                'attribute' => [
                    'attr' => [
                        'id' => 0,
                        'unit' => '',
                        'name' => ''
                    ],

                    'values' => [
                        'value' => [
                            'attr' => [
                                'id' => 0,
                                'attributeId' => 0,

                            ]
                        ]
                    ]
                ]
            ],

            'productTypes' => [
                'productType' => [
                    'attr' => [
                        'id' => 0,
                        'name' => ''
                    ],

                    'attributes' => [
                        'attribute' => [
                            'attr' => [
                                'id' => 0
                            ]
                        ]
                    ]
                ]
            ],

            'products' => [
                'product' => [
                    'attr' => [
                        'id' => 0
                    ],
                    'url',
                    'name',
                    'categoryId',
                    'brandId',
                    'productTypeId',
                    'description',
                    'sku',
                    'barcode',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                    'video',
                    'images' => [
                        'image' => [
                            'attr' => [
                                'id' => 0,
                                'title' => ''
                            ],
                        ]
                    ],
                    'attributeValues' => [
                        'attributeValue' => [
                            'attr' => [
                                'attributeId' => 0,
                                'valueId' => 0
                            ],
                        ]
                    ],
                    'reviews' => [
                        'review' => [
                            'attr' => [
                                'id' => 0
                            ],
                            'rating',
                            'name',
                            'comment',
                            'publishedAt'
                        ]
                    ]
                ]
            ]
        ]
];