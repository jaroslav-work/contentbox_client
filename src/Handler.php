<?php
/**
 * Created by PhpStorm.
 * User: jaroslaw
 * Date: 15.06.17
 * Time: 10:41
 */

namespace ContentBox;
use Exception;
use ContentBox\Client;
class Handler
{
    public $apiAccessToken = 'RRdSsaL3QB2hgPXVctKwrFfs6vuPDEbN';
    private $shopId = 186;
    private $_client;
    public $apiUrl = 'https://contentbox.ru/api/v2';

    public function getNewItems()
    {
        $client = $this->getClient();
        try {
            $response = $client->get($this->getApiUrl('work/items',
                ['sync_status' => 1, 'status' => 3, 'per-page' => 50, 'access-token' => $this->apiAccessToken]));
            return $response->getBody();
        } catch (Exception $e) {
            throw  $e;
        }
    }

    protected function getClient()
    {
        if ($this->_client) {
            return $this->_client;
        }
        $this->_client = new Client();
        return $this->_client;
    }

    protected function getApiUrl($route, $params = [])
    {
        return sprintf('%s/%s?shop_id=%s&%s', $this->apiUrl, $route, $this->shopId, http_build_query($params));
    }


    public function markWorkItemSynced(array $item)
    {
        try {
            $result = $this->getClient()->put($this->getApiUrl('work/items/' . $item['id'] . '/set-sync-status'), [
                'shop_id' => $this->shopId,
                'access-token' => $this->apiAccessToken
            ],
                [ 'value' => 1]
            );

            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function sync()
    {
        $items = $this->getNewItems();
        foreach ($items as $item) {
            $result = false;
            try {
                try {
                    switch ((int)$item['type']) {
                        case 1:
                            var_dump('Headers');
                            break;
                        case 2:
                            var_dump('Attributes');
                            break;
                        case 3:
                            var_dump('Description');
                            break;
                        case 4:
                            var_dump('Video');
                            break;
                        case 5:
                            var_dump('Images');
                            break;
                        case 6:
                            var_dump('Brand');
                            break;
                        case 7:
                            var_dump('Name');
                            break;
                        case 8:
                            var_dump('Meta Tags');
                            break;
                        case 17:
                            var_dump('Reviews');
                            break;
                    }
                    if (!$result) {
                        throw new Exception('Не удалось синхронизировать элемент: ' . json_encode($item));
                    }
                } catch (Exception $e) {
                    // если продукт не найден, то все нормально
                }
                $this->markWorkItemSynced($item);
            } catch (Exception $e) {
                throw $e;
            }
        }
    }



}