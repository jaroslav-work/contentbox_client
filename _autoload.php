<?php
/**
 * Created by PhpStorm.
 * User: jaroslaw
 * Date: 27.06.17
 * Time: 10:39
 */

spl_autoload_register(function ($class_name) {
    // project-specific namespace prefix
    $prefix = 'ContentBox\\';

    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/src/';

    $len = strlen($prefix);

    if (strncmp($prefix, $class_name, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class_name, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});
