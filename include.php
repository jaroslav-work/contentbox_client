<?
IncludeModuleLangFile(__FILE__);
Class CMoxielabContentbox {
    function update($id, $arFields) {

        //CMoxielabContentbox::d($arFields);
        CModule::Includemodule('iblock');
        $MODULE_ID = basename(dirname(__FILE__));
        $iblock_id = COption::GetOptionString($MODULE_ID, 'catalog_iblock_id', 0);
        $bxElement = CIBlockElement::GetByID($id)->GetNext();
        $el = new CIBlockElement;
        $ibp = new CIBlockProperty;
        if ($bxElement['ID'] > 0) {
            foreach ($arFields['changed'] as $change) {
                switch ($change) {
                    case 'name':
                        $res = $el->Update($bxElement['ID'], Array('NAME' => $arFields['name']));
                        break;
                    case 'meta_title':
                        $prop_id = COption::GetOptionString($MODULE_ID, 'title_property_id', 0);
                        if ($prop_id > 0) {
                            $main_props_to_update[$prop_id] = $arFields['meta_title'];
                        }
                        break;
                    case 'meta_keywords':
                        $prop_id = COption::GetOptionString($MODULE_ID, 'keyword_property_id', 0);
                        if ($prop_id > 0) {
                            $main_props_to_update[$prop_id] = $arFields['meta_keywords'];
                        }
                        break;
                    case 'h1':
                        $prop_id = COption::GetOptionString($MODULE_ID, 'h1_property_id', 0);
                        if ($prop_id > 0) {
                            $main_props_to_update[$prop_id] = $arFields['h1'];
                        }
                        break;
                    case 'h2':
                        $prop_id = COption::GetOptionString($MODULE_ID, 'h2_property_id', 0);
                        if ($prop_id > 0) {
                            $main_props_to_update[$prop_id] = $arFields['h2'];
                        }
                        break;
                    case 'eav':
                        foreach ($arFields['eav'] as $property) {
                            if (intval($property['attribute']['external_id']) > 0) {
                                $props_to_update[$property['attribute']['external_id']] = $property['value'];
                            } else {
                                $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock_id, 'NAME'=>$property['attribute']['name']));
                                if ($arProperty = $properties->GetNext()) {
                                    $props_to_update[$arProperty['ID']] = $property['value'];
                                } else {
                                    $PropID = $ibp->Add(Array(
                                        "NAME" => $property['attribute']['name'],
                                        "ACTIVE" => "Y",
                                        "SORT" => "100",
                                        "CODE" => strtoupper(Cutil::translit($property['attribute']['name'], "ru", array("replace_space"=>"_","replace_other"=>"_"))),
                                        "PROPERTY_TYPE" => "S",
                                        "IBLOCK_ID" => $iblock_id
                                    ));
                                    $props_to_update[$PropID] = $property['value'];
                                }
                            }
                        }
                        break;
                    case 'description':
                        $res = $el->Update($bxElement['ID'], Array('DETAIL_TEXT' => $arFields['description'], 'DETAIL_TEXT_TYPE' => 'html'));
                        break;
                    case 'video':
                        $prop_id = COption::GetOptionString($MODULE_ID, 'video_property_id', 0);
                        if ($prop_id > 0) {
                            $main_props_to_update[$prop_id] = $arFields['video'];
                        }
                        break;
                    case 'images':
                        $prop_id = COption::GetOptionString($MODULE_ID, 'more_photos_property_id', 0);
                        if ($prop_id > 0) {
                            $images_to_update[$prop_id] = $arFields['images'];
                        }
                        break;
                    case 'brand':
                        $prop_id = COption::GetOptionString($MODULE_ID, 'brand_property_id', 0);
                        if ($prop_id > 0) {
                            $brand_to_update = Array(
                                'ID' => $prop_id,
                                'VALUE' => $arFields['brand']
                            );
                        }
                        break;
                    default:
                        break;
                }
            }
            $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock_id));
            while ($property = $properties->GetNext()) {
                if (in_array($property['ID'], array_keys($main_props_to_update))) {
                    $propsUpdateArray[$property['CODE']] = $main_props_to_update[$property['ID']];
                }
                if (isset($brand_to_update) && $property['ID'] == $brand_to_update['ID']) {
                    if ($property['PROPERTY_TYPE'] == 'L' || $property['PROPERTY_TYPE'] == 'E') {
                        if (!empty($brand_to_update['VALUE']['external_id'])) {
                            $propsUpdateArray[$property['CODE']] = $brand_to_update['VALUE']['external_id'];
                        }
                    } else {
                        if (!empty($brand_to_update['VALUE']['name'])) {
                            $propsUpdateArray[$property['CODE']] = $brand_to_update['VALUE']['name'];
                        }
                    }
                }
                if (in_array($property['ID'], array_keys($props_to_update))) {
                    if ($property['PROPERTY_TYPE'] == 'L' || $property['PROPERTY_TYPE'] == 'E') {
                        if (!empty($props_to_update[$property['ID']]['external_id'])) {
                            $propsUpdateArray[$property['CODE']] = $props_to_update[$property['ID']]['external_id'];
                        }
                    } else {
                        if (!empty($props_to_update[$property['ID']]['value'])) {
                            $propsUpdateArray[$property['CODE']] = $props_to_update[$property['ID']]['value'];
                        }
                    }
                }
            }
            if (count($propsUpdateArray) > 0) {
                CIBlockElement::SetPropertyValuesEx($bxElement['ID'], false, $propsUpdateArray);
            }
            $queryParams = Array(
                'access-token' => COption::GetOptionString($MODULE_ID, 'access_token', ''),
                'shop_id' => COption::GetOptionString($MODULE_ID, 'shop_id', ''),
                'sync_status' => 0,
                'id' => $arFields['id']
            );
            CMoxielabContentbox::apiConnect($queryParams, 'PUT');

        } else {
            $return = Array(
                'STATUS' => 'FAIL',
                'MESSAGE' => 'Element not found',
            );
        }
        return $return;
    }

}
?>