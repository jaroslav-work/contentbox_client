<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/prolog.php");

if(!$USER->IsAdmin())
	return;
function d($arr) {
	print '<pre>';
	print_r ($arr);
	print '</pre>';
}
CModule::Includemodule('iblock');

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);
$MODULE_ID = 'moxielab.contentbox';
$APPLICATION->SetTitle(GetMessage("TITLE"));
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");

$arAllOptions = array(
	array("catalog_iblock_id", GetMessage("SELECT_IBLOCK_ID"), "", array("text", 0)),
	array("export_period", GetMessage("EXPORT_PERIOD"), "", array("text", 0)),
	array("brand_property_id", GetMessage("BRAND_PROPERTY"), "", array("property", 0)),
	array("title_property_id", GetMessage("TITLE_PROPERTY"), "", array("property", 0)),
	array("description_property_id", GetMessage("DESCRIPTION_PROPERTY"), "", array("property", 0)),
	array("keyword_property_id", GetMessage("KEYWORDS_PROPERTY"), "", array("property", 0)),
	array("more_photos_property_id", GetMessage("MORE_PHOTOS_PROPERTY"), "", array("property", 0)),
	array("video_property_id", GetMessage("VIDEO_PROPERTY"), "", array("property", 0)),
	array("export_link", GetMessage("EXPORT_LINK"), "", array("text", 'contentbox.xml')),
);
$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "ib_settings", "TITLE" => GetMessage("MAIN_TAB_SET")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults.$Export)>0 && check_bitrix_sessid()) {
	if(strlen($RestoreDefaults)>0) {
		COption::RemoveOption($MODULE_ID);
	} else {
		if (is_array($_REQUEST['checked_props'])) {
			COption::SetOptionString($MODULE_ID, 'checked_props', implode(',', $_REQUEST['checked_props']), 0);
		}
		if (COption::GetOptionString($MODULE_ID, 'export_period', 0) != $_REQUEST['export_period'] && $_REQUEST['export_period'] > 0) {
			CAgent::RemoveAgent("CMoxielabContentbox::GenerateXML();", $MODULE_ID);
			CAgent::AddAgent(
				"CMoxielabContentbox::GenerateXML();",
				$MODULE_ID, "N", $_REQUEST['export_period']*60, "", "Y", "", 30
			);
		} elseif ($_REQUEST['export_period'] == 0) {
			CAgent::RemoveAgent("CMoxielabContentbox::GenerateXML();", $MODULE_ID);
		}
		foreach($arAllOptions as $arOption) {
			$name=$arOption[0];
			$val=$_REQUEST[$name];
			if($arOption[2][0]=="checkbox" && $val!="Y")
				$val="N";
			COption::SetOptionString($MODULE_ID, $name, $val, $arOption[1]);
		}
	}
	
	if (strlen($Export)>0) {
		CMoxielabContentbox::GenerateXML();
		$exp = 1;
	} else {
		$exp = 0;
	}
	
	if(strlen($Update)>0 && strlen($_REQUEST["back_url_settings"])>0)
		LocalRedirect($_REQUEST["back_url_settings"]);
	else
		LocalRedirect($APPLICATION->GetCurPage()."?export=".$exp."&mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
}

$db_iblock_type = CIBlockType::GetList();
while($ar_iblock_type = $db_iblock_type->Fetch()) {
	$iblock_types[$ar_iblock_type['ID']] = Array(
		'ID' => $ar_iblock_type['ID'],
		'NAME' => CIBlockType::GetByIDLang($ar_iblock_type["ID"], LANG)['NAME'],
		'IBLOCKS' => Array(),
	);
	$res = CIBlock::GetList(Array(), Array('TYPE'=>$ar_iblock_type['ID'], 'ACTIVE'=>'Y'), false);
	while($ar_res = $res->Fetch()) {
		$iblock_types[$ar_iblock_type['ID']]['IBLOCKS'][] = Array(
			'ID' => $ar_res['ID'],
			'NAME' => $ar_res['NAME'],
		);
	}
}
$selected_catalog_iblock_id = COption::GetOptionString($MODULE_ID, 'catalog_iblock_id', 0);
$checked_props = explode(',', COption::GetOptionString($MODULE_ID, 'checked_props', 0));
if ($selected_catalog_iblock_id > 0) {
	$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$selected_catalog_iblock_id));
	while ($prop_fields = $properties->GetNext()) {
		$iblock_properties[] = Array(
			'ID' => $prop_fields["ID"],
			'NAME' => $prop_fields["NAME"],
			'CHECKED' => in_array($prop_fields['ID'], $checked_props)?'Y':'N'
		);
	}
}
if($export == 1) {
	print '
<div id="tbl_iblock_export_result_div">	

	<div class="adm-info-message-wrap adm-info-message-green">
		<div class="adm-info-message">
			<div class="adm-info-message-title">'.GetMessage("EXPORT_SUCCESS").'</div>
			<a href="http://'.$_SERVER['SERVER_NAME'].'/upload/'.COption::GetOptionString($MODULE_ID, 'export_link', 'contentbox.xml').'" target="_blank">http://'.$_SERVER['SERVER_NAME'].'/upload/'.COption::GetOptionString($MODULE_ID, 'export_link', 'contentbox.xml').'</a><br>
			'.GetMessage("EXPORT_SUCCESS_EXT").'
			<div class="adm-info-message-icon"></div>
		</div>
	</div>
</div>
	';
}
?>
<?/*
	<div class="adm-info-message-wrap adm-info-message-red">
		<div class="adm-info-message">
			<div class="adm-info-message-title">Ошибка открытия файла.</div>
			
			<div class="adm-info-message-icon"></div>
		</div>
	</div>
*/?>


<?$tabControl->Begin();?>
<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?echo LANGUAGE_ID?>">
<?$tabControl->BeginNextTab();?>
	<tr class="heading" id="tr_IBLOCK_ELEMENT_PROPERTIES"><td colspan="2"><?=GetMessage('IBLOCK_PROPERTIES')?>:</td></tr>
	<tr>
		<td width="40%" nowrap>
			<label for="catalog_iblock_id"><?=GetMessage('SELECT_IBLOCK_ID')?>:</label>
		</td>
		<td width="60%">
			<select name="catalog_iblock_id" id="catalog_iblock_id">
				<option value="0"><?=GetMessage('SELECT_IBLOCK')?></option>
<?	foreach ($iblock_types as $type):?>
<?		if (!empty($type['IBLOCKS'])):?>
				<optgroup label="<?=$type['NAME']?>">
<?			foreach ($type['IBLOCKS'] as $iblock):?>
					<option <?if ($selected_catalog_iblock_id == $iblock['ID']):?>selected <?endif?>value="<?=$iblock['ID']?>"><?=$iblock['NAME']?></option>
<?			endforeach?>
				</optgroup>
<?		endif?>
<?	endforeach?>
			</select>
		</td>
	</tr>
<?	if ($selected_catalog_iblock_id > 0):?>
<?		foreach ($arAllOptions as $option):?>
<?			if ($option[3][0] != 'property') continue;?>
	<tr>
		<td width="40%" nowrap>
			<label for="<?=$option[0]?>"><?=$option[1]?>:</label>
		</td>
		<td width="60%">
			<select name="<?=$option[0]?>" id="<?=$option[0]?>">
				<option value="0"><?=GetMessage('PROPERTY_SELECT')?></option>
<?		foreach ($iblock_properties as $prop):?>
				<option <?if (COption::GetOptionString($MODULE_ID, $option[0], 0) == $prop['ID']):?>selected <?endif?>value="<?=$prop['ID']?>"><?=$prop['NAME']?> [<?=$prop['ID']?>]</option>
<?		endforeach?>
			</select>
		</td>
	</tr>
<?		endforeach?>
<?	endif?>
	<tr class="heading" id="tr_IBLOCK_ELEMENT_PROPERTIES"><td colspan="2"><?=GetMessage('EXPORT_PROPERTIES')?>:</td></tr>
	<tr>
		<td width="40%" nowrap>
			<label for="export_period"><?=GetMessage('EXPORT_PERIOD')?>:</label>
		</td>
		<td width="60%">
			<input type="text" size="3" maxlength="255" value="<?=COption::GetOptionString($MODULE_ID, 'export_period', 0)?>" id="export_period" name="export_period"> <?=GetMessage('EXPORT_PERIOD_EXT')?>
		</td>
	</tr>
	<tr>
		<td width="40%" nowrap>
			<label for="export_link"><?=GetMessage('EXPORT_LINK')?>: http://<?=$_SERVER['SERVER_NAME']?>/upload/</label>
		</td>
		<td width="60%">
			<input type="text" size="40" maxlength="255" value="<?=COption::GetOptionString($MODULE_ID, 'export_link', 'contentbox.xml')?>" id="export_link" name="export_link">
		</td>
	</tr>

<?$tabControl->Buttons();?>
	<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
	<input type="submit" name="Apply" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
	<input type="submit" name="Export" value="<?=GetMessage("APPLY_EXPORT")?>" title="<?=GetMessage("APPLY_EXPORT")?>" class="adm-btn-save" style="float:right">
	<?=bitrix_sessid_post();?>
<?$tabControl->End();?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");?>