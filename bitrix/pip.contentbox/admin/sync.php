<?

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ?????? ????? ??????
$POST_RIGHT = $APPLICATION->GetGroupRight("sale");
if ($POST_RIGHT == "D") {
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
CModule::Includemodule('iblock');
CModule::Includemodule('moxielab.contentbox');
IncludeModuleLangFile(__FILE__);
$MODULE_ID = 'moxielab.contentbox';
global $DB;
$sTableID = "tbl_ready_items";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);
$lAdmin->onLoadScript="onLoadTable();";
$FilterArr = Array(
    "id",
    "name",
);
$lAdmin->InitFilter($FilterArr);
$queryParams = Array(
	'access-token' => COption::GetOptionString($MODULE_ID, 'access_token', ''),
	'shop_id' => COption::GetOptionString($MODULE_ID, 'shop_id', ''),
//	'sync_status' => 1,
	'status' => 3,
    'per-page' => 50
);

$types = Array(
    1 =>    'Headers',
    2 =>    'Attributes',
    3 =>    'Description',
    4 =>    'Video',
    5 =>    'Images',
    6 =>    'Brand',
    7 =>    'Name',
    8 =>    'Meta_tags',
    17 => 'Review'
);
//echo '<pre>';
$items = json_decode(CMoxielabContentbox::apiConnect($queryParams, "GET"), true);
/*var_dump($items);
die();*/
foreach ($items as $item) {
    foreach ($item as $key=>$value) {
		if ($value == false) $item[$key] = '';
	}
	$jsItems[$item['source']['external_id']] = $item;
}
//die();

if ($_REQUEST['action'] == 'apply') {

	if ($_REQUEST['action_target'] == 'selected') {
		foreach ($items as $item) {
			$res = CMoxielabContentbox::update($item['source']['external_id'], $item);
			if ($res['STATUS'] == 'FAIL') {
				$errors[] = $res['MESSAGE'];
			}
			unset($jsItems[$item['source']['external_id']]);
		}
	} else {
		foreach ($_REQUEST['ID'] as $id) {
			if (isset($jsItems[$id])) {
				$res = CMoxielabContentbox::update($id, $jsItems[$id]);
				if ($res['STATUS'] == 'FAIL') {
					$errors[] = $res['MESSAGE'];
				}
			}
			unset($jsItems[$id]);
		}
	}
}
if (count($jsItems) > 0) {
	$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>COption::GetOptionString($MODULE_ID, 'catalog_iblock_id', 0), "ID"=>array_keys($jsItems)), false, false, Array("ID", "NAME", "DETAIL_PAGE_URL"));
	while($ob = $res->GetNext()) {
        $bxItems[$ob['ID']] = $ob;
    }

	foreach ($items as $item) {
		$changed_info = Array();
		$new_properties = Array();

		foreach ($item['source'] as $prop) {
			if (intval($prop['external_id']) == 0) {
				$new_properties[] = $prop['name'];
			}
		}


		if (count($item['result']['images']) > 0) {
			$images = '';
			foreach ($item['result']['images'] as $image) {
				$images .= "
					<div class=\"adm-input-file-control\" style=\"float:left; margin-right:2px;\">
						<span class=\"adm-input-file-exist-cont\">
							<div class=\"adm-input-file-ex-wrap\">
								<span class=\"adm-input-file-preview\" style=\"min-width: 1px; min-height:1px;\">
									<a title=\"".GetMessage("MOXIELAB_CONTENTBOX_SYNC_TABLE_IMAGE_ZOOM")."\" onclick=\"ImgShw('".$image['url']."','800','800', ''); return false;\" href=\"".$image['url']."\" target=\"_blank\"><img src=\"".$image['url']."\" alt=\"\" height=\"50\" border=\"0\"></a>
								</span>
							</div>
						</span>
					</div>
				";
			}
		}


		if (isset($bxItems[$item['source']['external_id']])) {
			$itemsArray[] = Array(
				'ID' => $item['source']['external_id'],
				'SYNC' => GetMessage("MOXIELAB_CONTENTBOX_SYNC_STATUS_". $item['sync_status']),
				'NAME' => '<a target="_blank" href="'.$bxItems[$item['source']['external_id']]['DETAIL_PAGE_URL'].'">'.$bxItems[$item['source']['external_id']]['NAME'].'</a>',
				'NEW_NAME' => '<a href="#" class="show_popup_item_info" data-id="'.$item['source']['external_id'].'" style="border-bottom:1px dotted">'.$item['source']['name'].'</a>',
				'CHANGED_INFO' => GetMessage("MOXIELAB_CONTENTBOX_SYNC_CHANGED_".$types[$item['type']]),
				'IMAGES' => $images,
			);
		}
	}
}
//CMoxielabContentbox::d($itemsArray);
/*
if (strlen($response) <= 0) {
   if (intval($errno)>0 || strlen($errstr)>0) {
	   print '
	<div class="adm-info-message-wrap adm-info-message-red">
		<div class="adm-info-message">
			<div class="adm-info-message-title">
			: ('.$errno.') '.$errstr.'
			</div>
			<div class="adm-info-message-icon"></div>
		</div>
	</div>';
   }
} else {
   print $strQueryText;
}
*/
if (isset($_REQUEST["mode"]) && $_REQUEST["mode"] == "excel") {
    $arNavParams = false;
} else {
    $arNavParams = array("nPageSize"=>CAdminResult::GetNavSize($sTableID, array('nPageSize' => 20, 'sNavID' => $APPLICATION->GetCurPage())));                                                                                                                                                                                                                                                           
}


$rsDbData = new CDBResult;
if (!empty($_REQUEST['by']) && !empty($_REQUEST['order'])) {
	if ($order == 'asc') {
		usort($itemsArray, function($a, $b) {
			global $by; return strcasecmp($a[$by], $b[$by]);
		});
	} else {
		usort($itemsArray, function($a, $b) {
			global $by; return strcasecmp($b[$by], $a[$by]);
		});
	}
}
if (isset($filter_name) && $del_filter != 'Y') {
	foreach ($itemsArray as $key=>$item) {
		if (strpos($item['NAME'], $filter_name) === false) unset($itemsArray[$key]);
	}
}
if (isset($filter_id) && $del_filter != 'Y') {
	foreach ($itemsArray as $key=>$item) {
		if ($item['ID'] != $filter_id) unset($itemsArray[$key]);
	}
}

$rsDbData->InitFromArray($itemsArray);
$rsData = new CAdminResult($rsDbData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("MOXIELAB_CONTENTBOX_SYNC_TABLE_ITEMS")));
$headers = array(
	array(
		"id"            => "ID",
		"content"        => "ID",
		"sort"             => "ID",
		"default"        => true,
	),

    array(
        "id"            => "SYNC",
        "content"        => "SYNC",
        "sort"             => "SYNC",
        "default"        => true,
    ),

	array(
		"id"            => "NAME",
		"content"        => GetMessage("MOXIELAB_CONTENTBOX_SYNC_TABLE_NAME"),
		"sort"             => "NAME",
		"default"        => true,
	),
	array(
		"id"            => "NEW_NAME",
		"content"        => GetMessage("MOXIELAB_CONTENTBOX_SYNC_TABLE_NEW_NAME"),
		"sort"             => "NEW_NAME",
		"default"        => true,
	),

	array(
		"id"            => "CHANGED_INFO",
		"content"        => GetMessage("MOXIELAB_CONTENTBOX_SYNC_TABLE_CHANGED_INFO"),
		"sort"             => false,
		"default"        => true,
	),

	array(
		"id"            => "IMAGES",
		"content"        => GetMessage("MOXIELAB_CONTENTBOX_SYNC_TABLE_IMAGES"),
		"sort"             => false,
		"default"        => true,
	),

);

$lAdmin->AddHeaders($headers);
while ($arRes = $rsData->NavNext(true, "f_")){
	$row =& $lAdmin->AddRow($arRes['ID'], $arRes);
	$row->AddViewField("IMAGES", htmlspecialcharsBack($arRes['IMAGES']));
	$row->AddViewField("NAME", htmlspecialcharsBack($arRes['NAME']));
	$row->AddViewField("NEW_NAME", '<b>'.$arRes['NEW_NAME'].'</b>');
	$row->AddViewField("CHANGED_INFO", htmlspecialcharsBack($arRes['CHANGED_INFO']));
	$arActions = Array();
	$arActions[] = array(
		"ICON"=>"move",
		"DEFAULT"=>true,
		"TEXT"=>GetMessage("MOXIELAB_CONTENTBOX_SYNC_VIEW_APPLY"),
		"ACTION"=>$lAdmin->ActionRedirect("moxielab.contentbox_sync.php?ID[]=".$f_ID."&action=apply")
	);
	$row->AddActions($arActions);
}
$lAdmin->AddFooter(
	array(
		array("title"=>"MAIN_ADMIN_LIST_SELECTED", "value"=>$rsData->SelectedRowsCount()), // ???-?? ?????????
		array("counter"=>true, "title"=>"MAIN_ADMIN_LIST_CHECKED", "value"=>"0"), // ??????? ????????? ?????????
	)
);
/*
// ?????????? ???? ?? ?????? ?????? - ?????????? ????????
$aContext = array(
  array(
    "TEXT"=>GetMessage("MOXIELAB_CONTENTBOX_SYNC_VIEW_APPLY_ALL"),
    "LINK"=>"moxielab.contentbox_sync.php?lang=".LANG,
    "TITLE"=>GetMessage("MOXIELAB_CONTENTBOX_SYNC_VIEW_APPLY_ALL"),
    "ICON"=>"btn",
  ),
);
$lAdmin->AddAdminContextMenu($aContext);
*/
//CMoxielabContentbox::d($_REQUEST);
$lAdmin->AddGroupActionTable(Array(
	"apply"=>GetMessage("MOXIELAB_CONTENTBOX_SYNC_VIEW_APPLY"), // ??????? ????????? ????????
));
$lAdmin->CheckListMode();
$APPLICATION->SetTitle(GetMessage("MOXIELAB_CONTENTBOX_SYNC_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$oFilter = new CAdminFilter($sTableID."_filter",
	array(
		"ID",
		GetMessage("MOXIELAB_CONTENTBOX_SYNC_FILTER_NAME"),
	)
);
CUtil::InitJSCore(array('ajax', 'jquery', 'popup'));
$APPLICATION->AddHeadScript('http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js');
?>
<div id="hideItemBlock" style="display:none;"></div>
<style>
	.itempreview {line-height: 24px; max-width: 800px; padding: 20px;}
	.itempreview > h1 {color: #333; font-size: 24px; font-weight: normal; margin: 0 0 20px;}
	.item_info > span {font-weight: bold;}
	.item_info_props {margin: 0 0 20px;}
	.item_info_props span {font-weight: bold;}
	.item_info {}
	.item_info_description p {font-size: 12px; line-height: 17px; margin: 0 0 10px;}
	.item_info_description > ul {font-size: 12px; line-height: 18px; margin: 0;}
	.popup-window {border-radius: 3px;}
</style>
<script>
	function onLoadTable() {
		var popupTemplate = '' +
		'	<div class="itempreview">' +
		'		<h1>${name}</h1>' +
		'		<div class="item_info"><span>meta_title</span> <?=GetMessageJS("MOXIELAB_CONTENTBOX_6")?></div>' +
		'		<div class="item_info"><span>meta_keywords</span> <?=GetMessageJS("MOXIELAB_CONTENTBOX_7")?></div>' +
		'		<div class="item_info"><span>h1</span> <?=GetMessageJS("MOXIELAB_CONTENTBOX_8")?></div>' +
		'		<div class="item_info"><span>h2</span> <?=GetMessageJS("MOXIELAB_CONTENTBOX_9")?></div>' +
		'		<div class="item_info"><span><?=GetMessage("MOXIELAB_CONTENTBOX_SYNC_TMPL_BRAND")?></span> <?=GetMessageJS("MOXIELAB_CONTENTBOX_10")?></div>' +
		'		<div class="item_info"><span><?=GetMessage("MOXIELAB_CONTENTBOX_SYNC_TMPL_VIDEO")?></span> <?=GetMessageJS("MOXIELAB_CONTENTBOX_")?><a href="${video}">${video}</a></div>' +
		'		<div class="item_info"><span><?=GetMessage("MOXIELAB_CONTENTBOX_SYNC_TMPL_PROPS")?></span></div>' +
		'		<ul class="item_info_props">' +
		'		{{each eav}}' +
		'			<li><span>${attribute.name}</span>: ${value.value}</li>' +
		'		{{/each}}' +
		'		</ul>' +
		'		<div class="item_info"><span><?=GetMessage("MOXIELAB_CONTENTBOX_SYNC_TMPL_DESCRIPTION")?></span></div>' +
		'		<div class="item_info_description">' +
		'			{{html description}}' +
		'		</div>' +
		'	</div>' +
		'';
		var items = <?=json_encode($jsItems)?>;
		console.log(items);
		BX.ready(function(){
			var oPopup = new BX.PopupWindow('call_feedback', window.body, {
				autoHide : true,
				offsetTop : 1,
				offsetLeft : 0,
				lightShadow : true,
				closeIcon : true,
				closeByEsc : true,
				overlay: {
					backgroundColor: 'black', opacity: '80'
				}
			});
			$('.show_popup_item_info').click(function() {
				$('#hideItemBlock').html('');
				 $.tmpl(popupTemplate, items[$(this).data('id')]).appendTo("#hideItemBlock");
				oPopup.setContent(BX('hideItemBlock'));
				oPopup.show();
				return false;
			});
		});
	}
	onLoadTable();
</script>
<form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
<?$oFilter->Begin();?>
	<tr>
		<td>ID</td>
		<td><input type="text" name= "filter_id" size="47" value="<?=$filter_id?>"></td>
	</tr>
	<tr>
		<td><?=GetMessage("MOXIELAB_CONTENTBOX_SYNC_FILTER_NAME")?></td>
		<td><input type="text" name= "filter_name" size="47" value="<?=$filter_name?>"></td>
	</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>
<?$lAdmin->DisplayList();?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>