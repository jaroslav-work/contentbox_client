<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/prolog.php");

if(!$USER->IsAdmin())
	return;

CModule::Includemodule('iblock');
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);
$MODULE_ID = 'moxielab.contentbox';
$APPLICATION->SetTitle(GetMessage("MOXIELAB_CONTENTBOX_MAIN_TAB_SET"));
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");

$arAllOptions = array(
	array("shop_id", GetMessage("MOXIELAB_CONTENTBOX_SHOP_ID"), "", array("text", '')),
	array("access_token", GetMessage("MOXIELAB_CONTENTBOX_ACCESS_TOKEN"), "", array("text", '')),
);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("MOXIELAB_CONTENTBOX_MAIN_TAB_SET"), "ICON" => "ib_settings", "TITLE" => GetMessage("MOXIELAB_CONTENTBOX_MAIN_TAB_SET")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults.$Export)>0 && check_bitrix_sessid()) {
	if(strlen($RestoreDefaults)>0) {
		COption::RemoveOption($MODULE_ID);
	} else {
		foreach($arAllOptions as $arOption) {
			$name=$arOption[0];
			$val=$_REQUEST[$name];
			if($arOption[2][0]=="checkbox" && $val!="Y")
				$val="N";
			COption::SetOptionString($MODULE_ID, $name, $val, $arOption[1]);
		}
	}

	if(strlen($Update)>0 && strlen($_REQUEST["back_url_settings"])>0)
		LocalRedirect($_REQUEST["back_url_settings"]);
	else
		LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
}
?>
<?$tabControl->Begin();?>
<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?echo LANGUAGE_ID?>">
<?$tabControl->BeginNextTab();?>
<?foreach ($arAllOptions as $option):?>
	<tr>
		<td width="40%" nowrap>
			<label for="<?=$option[0]?>"><?=$option[1]?>:</label>
		</td>
		<td width="60%">
			<input type="text" size="40" maxlength="255" value="<?=COption::GetOptionString($MODULE_ID, $option[0], '')?>" id="<?=$option[0]?>" name="<?=$option[0]?>">
		</td>
	</tr>
<?endforeach?>
	<tr id="composite_button_disclaimer_row">
		<td width="40%" nowrap>
		</td?>
		<td width="60%">
			<div class="adm-info-message-wrap">
				<div class="adm-info-message">
					<b><?=GetMessage("MOXIELAB_CONTENTBOX_TOKEN_INFO")?></b>
				</div>
			</div>
		</td>
	</tr>
<?$tabControl->Buttons();?>
	<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
	<input type="submit" name="Apply" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
	<?=bitrix_sessid_post();?>
<?$tabControl->End();?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");?>