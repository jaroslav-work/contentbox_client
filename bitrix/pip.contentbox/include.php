<?
IncludeModuleLangFile(__FILE__);

Class CMoxielabContentbox
{
    public $types = Array(
        1 => 'Headers',
        2 => 'Attributes',
        3 => 'Description',
        4 => 'Video',
        5 => 'Images',
        6 => 'Brand',
        7 => 'Name',
        8 => 'Meta-tags',
        17 => 'Review'
    );

    function update($id, $arFields)
    {
        CModule::Includemodule('iblock');
        $MODULE_ID = basename(dirname(__FILE__));
        $iblock_id = COption::GetOptionString($MODULE_ID, 'catalog_iblock_id', 0);
        $bxElement = CIBlockElement::GetByID($id)->GetNext();

        $el = new CIBlockElement;
        $ibp = new CIBlockProperty;

        if ($bxElement['ID'] > 0) {
            switch ($arFields['type']) {
                case 5: //images
                    $prop_id = COption::GetOptionString($MODULE_ID, 'more_photos_property_id', 0);
                    if ($prop_id > 0) {
                        $images_props_to_save = [];
                        foreach ($arFields['result']['images'] as $image) {
                            $arFile = CFile::MakeFileArray($image['url']);
                            $images_props_to_save[$prop_id][] = $arFile;
                        }

                    }

                    break;
                case 7: //name
                    $res = $el->Update($bxElement['ID'], Array('NAME' => $arFields['result']['name']));
                    break;

                case 3:
                    $res = $el->Update($bxElement['ID'],
                        Array('DETAIL_TEXT' => $arFields['result']['description'], 'DETAIL_TEXT_TYPE' => 'html'));
                    break;

                case 4:
                    $prop_id = COption::GetOptionString($MODULE_ID, 'video_property_id', 0);
                    if ($prop_id > 0) {
                        foreach ($arFields['result'] as $video) {
                            $main_props_to_update[$prop_id][] = $video;
                        }
                    }
                    break;
                case 8:
                    $prop_ids['meta_title'] = COption::GetOptionString($MODULE_ID, 'title_property_id', 0);
                    $prop_ids['meta_description'] = COption::GetOptionString($MODULE_ID, 'description_property_id', 0);
                    $prop_ids['meta_keywords'] = COption::GetOptionString($MODULE_ID, 'keyword_property_id', 0);


                    foreach ($prop_ids as $k => $prop_id) {
                        if ($prop_id > 0 && isset($arFields['result'][$k])) {
                            $main_props_to_update[$prop_id] = $arFields['result'][$k];
                        } else {

                            die('DUMP');
                        }
                    }
                    break;

                default:
                    break;
            }

            $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"),
                Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock_id));

            $propsUpdateArray = [];

            while ($property = $properties->GetNext()) {
                if (in_array($property['ID'], array_keys($main_props_to_update))) {
                    $propsUpdateArray[$property['CODE']] = $main_props_to_update[$property['ID']];
                }

                if (!empty($images_props_to_save)) {
                    if (in_array($property['ID'], array_keys($images_props_to_save))) {
                        foreach ($images_props_to_save[$property['ID']] as $image) {
                            CIBlockElement::SetPropertyValues(
                                $bxElement['ID'],
                                $iblock_id,
                                $image,
                                $property['CODE']);
                        }
                    }
                }
            }

            if (count($propsUpdateArray) > 0) {
                CIBlockElement::SetPropertyValuesEx($bxElement['ID'], false, $propsUpdateArray);
            }

            $queryParams = Array(
                'access-token' => COption::GetOptionString($MODULE_ID, 'access_token', ''),
                'shop_id' => COption::GetOptionString($MODULE_ID, 'shop_id', ''),
                'sync_status' => 0,
                'id' => $arFields['id']
            );

            CMoxielabContentbox::apiConnect($queryParams, 'PUT');

            if (count($propsUpdateArray) > 0) {
                CIBlockElement::SetPropertyValuesEx($bxElement['ID'], false, $propsUpdateArray);
            }

            $queryParams = Array(
                'access-token' => COption::GetOptionString($MODULE_ID, 'access_token', ''),
                'shop_id' => COption::GetOptionString($MODULE_ID, 'shop_id', ''),
                'sync_status' => 0,
                'id' => $arFields['id']
            );
            CMoxielabContentbox::apiConnect($queryParams, 'PUT');

        } else {
            $return = Array(
                'STATUS' => 'FAIL',
                'MESSAGE' => 'Element not found',
            );
        }
        return $return;
    }

    function d($arr)
    {
        print '<pre>';
        print_r($arr);
        print '</pre>';
    }

    function strip($b1)
    {
        $b1 = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $b1);
        $b1 = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $b1);
        return $b1;
    }

    function apiConnect($queryParams, $method)
    {
        $response = [];
        if ($method == "GET") {
            $url = 'https://contentbox.ru/api/v2/work/items?' . http_build_query($queryParams);
            if (function_exists('curl_version')) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec($ch);
                curl_close($ch);
            } else {
                $arrContextOptions = array(
                    "ssl" => array(
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                    ),
                );
                $response = file_get_contents($url, false, stream_context_create($arrContextOptions));
            }


        } elseif ($method == 'PUT') {
            $url = 'https://contentbox.ru/api/v2/work/items/' . $queryParams['id'] . '/set-sync-status?' . http_build_query(Array(
                    'access-token' => $queryParams['access-token'],
                    'shop_id' => $queryParams['shop_id'],
                ));
            $data_string = json_encode(Array('value' => 1));
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ));
            $response = curl_exec($ch);
            curl_close($ch);
        }
        return $response;
    }


    function GenerateXML()
    {
        echo '<pre>';
        $MODULE_ID = basename(dirname(__FILE__));
        $xml = '<catalog date="' . date('Y-m-d H:i') . '">' . PHP_EOL;
        $xml .= '	<categories>' . PHP_EOL;
        CModule::Includemodule('iblock');
        $iblock_id = COption::GetOptionString($MODULE_ID, 'catalog_iblock_id', 0);
        if ($iblock_id > 0) {
            $db_list = CIBlockSection::GetList(Array('LEFT_MARGIN' => 'ASC'),
                Array('IBLOCK_ID' => $iblock_id, 'GLOBAL_ACTIVE' => 'Y'));
            while ($ar_result = $db_list->GetNext()) {
                if ($ar_result['IBLOCK_SECTION_ID'] > 0) {
                    $parentID = ' parentId="' . $ar_result['IBLOCK_SECTION_ID'] . '"';
                } else {
                    $parentID = '';
                }
                $xml .= '		<category id="' . $ar_result['ID'] . '"' . $parentID . '>' . $ar_result['NAME'] . '</category>' . PHP_EOL;
            }
        }
        $xml .= '	</categories>' . PHP_EOL;

        $mainProperties = Array('brand' => Array(), 'title' => Array(), 'keyword' => Array(), 'more_photos' => Array());
        foreach ($mainProperties as $code => $main_property) {
            $mainProperties[$code]['ID'] = COption::GetOptionString($MODULE_ID, $code . '_property_id', 0);
            if ($mainProperties[$code]['ID'] == 0) {
                unset($mainProperties[$code]);
            } else {
                $mainPropertiesIDs[] = $mainProperties[$code]['ID'];
            }
        }

        $arSelect = Array(
            "ID",
            "NAME",
            "IBLOCK_ID",
            "DETAIL_TEXT",
            "DETAIL_PICTURE",
            "DETAIL_PAGE_URL",
            "IBLOCK_SECTION_ID"
        );

        $checked_props = explode(',', COption::GetOptionString($MODULE_ID, 'checked_props', 0));
        $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"),
            Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock_id));
        while ($prop_fields = $properties->GetNext()) {
            $propertiesInfo[$prop_fields['CODE']] = $prop_fields;
            foreach ($mainProperties as $code => $main_property) {
                if ($prop_fields['ID'] == $main_property['ID']) {
                    $mainProperties[$code]['CODE'] = $prop_fields['CODE'];
                }
            }
            if ($prop_fields['MULTIPLE'] == 'Y') {
                continue;
            }
            if (in_array($prop_fields['ID'], $checked_props) && !in_array($prop_fields['ID'], $mainPropertiesIDs)) {
                $selectedProps[] = $prop_fields['CODE'];
            }
            if (in_array($prop_fields['ID'], $checked_props) || in_array($prop_fields['ID'], $mainPropertiesIDs)) {
                $arSelect[] = "PROPERTY_" . $prop_fields['CODE'];
            }
            if ($prop_fields['PROPERTY_TYPE'] == 'S' || $prop_fields['PROPERTY_TYPE'] == 'N') {
                $stringProps[] = $prop_fields['CODE'];
            } elseif ($prop_fields['PROPERTY_TYPE'] == 'L') {
                $listProps[] = $prop_fields['ID'];
                $db_enum_list = CIBlockProperty::GetPropertyEnum($prop_fields['ID'], Array(),
                    Array("IBLOCK_ID" => $iblock_id));
                while ($ar_enum_list = $db_enum_list->GetNext()) {
                    $listPropsValues[$prop_fields['CODE']]['NAME'] = $prop_fields['NAME'];
                    $listPropsValues[$prop_fields['CODE']]['ID'] = $prop_fields['ID'];
                    $listPropsValues[$prop_fields['CODE']]['VALUES'][] = Array(
                        'ID' => $ar_enum_list["ID"],
                        'VALUE' => $ar_enum_list["VALUE"]
                    );
                }
            } elseif ($prop_fields['PROPERTY_TYPE'] == 'E') {
                $listProps[] = $prop_fields['ID'];
                $res = CIBlockElement::GetList(Array(), Array('IBLOCK_ID' => $prop_fields['LINK_IBLOCK_ID']), false,
                    false, Array('NAME', 'ID'));
                while ($ob = $res->GetNext()) {
                    $listPropsValues[$prop_fields['CODE']]['NAME'] = $prop_fields['NAME'];
                    $listPropsValues[$prop_fields['CODE']]['ID'] = $prop_fields['ID'];
                    $listPropsValues[$prop_fields['CODE']]['VALUES'][] = Array(
                        'ID' => $ob["ID"],
                        'VALUE' => $ob["NAME"]
                    );
                }
            }
        }

        $arFilter = Array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y");

        $res = CIBlockElement::GetList(Array('ID' => 'ASC'), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNext()) {
            $section_id = $ob['IBLOCK_SECTION_ID'];
            if (is_null($section_id)) {
                $section_id = 0;
            }
            $item = Array(
                'ID' => $ob['ID'],
                'NAME' => $ob['NAME'],
                'URL' => 'http://' . COption::GetOptionString('main', 'server_name', 0) . $ob['DETAIL_PAGE_URL'],
                'DESCRIPTION' => $ob['DETAIL_TEXT'],
                'SKU' => $ob['ID'],
                'SECTION' => $ob['IBLOCK_SECTION_ID'],
                'BRAND' => $ob['PROPERTY_' . $mainProperties['brand']['CODE'] . '_VALUE'],
                'KEYWORDS' => $ob['PROPERTY_' . $mainProperties['keyword']['CODE'] . '_VALUE'],
                'TITLE' => htmlspecialchars($ob['NAME'], ENT_XML1),
                'IMAGES' => Array(),
                'PROPERTIES' => Array()
            );
            if ($ob['DETAIL_PICTURE'] > 0) {
                $item['IMAGES'][] = Array(
                    'ID' => $ob['DETAIL_PICTURE'],
                    'LINK' => 'http://' . COption::GetOptionString('main', 'server_name',
                            0) . CFile::GetPath($ob['DETAIL_PICTURE']),
                );
            }
            foreach ($selectedProps as $selectedProp) {
                $item['PROPERTIES'][$selectedProp] = '';
            }
            if (isset($mainProperties['more_photos']['CODE'])) {
                $db_props = CIBlockElement::GetProperty($iblock_id, $ob['ID'], array("sort" => "asc"),
                    Array("CODE" => $mainProperties['more_photos']['CODE']));
                while ($ar_props = $db_props->Fetch()) {
                    if ($ar_props['VALUE'] > 0) {
                        $item['IMAGES'][] = Array(
                            'ID' => $ar_props['VALUE'],
                            'LINK' => 'http://' . COption::GetOptionString('main', 'server_name',
                                    0) . CFile::GetPath($ar_props['VALUE']),
                        );
                    }
                }
            }
            if (in_array($mainProperties['brand']['CODE'], $stringProps)) {
                if (!empty($ob['PROPERTY_' . $mainProperties['brand']['CODE'] . '_VALUE'])) {
                    $stringPropsValues[$mainProperties['brand']['CODE']]['NAME'] = $propertiesInfo[$mainProperties['brand']['CODE']]['NAME'];
                    $stringPropsValues[$mainProperties['brand']['CODE']]['ID'] = $propertiesInfo[$mainProperties['brand']['CODE']]['ID'];
                    $stringPropsValues[$mainProperties['brand']['CODE']]['VALUES_ENUM'][] = $ob['PROPERTY_' . $mainProperties['brand']['CODE'] . '_VALUE'];
                    $stringPropsValues[$mainProperties['brand']['CODE']]['VALUES_ENUM'] = array_unique($stringPropsValues[$mainProperties['brand']['CODE']]['VALUES_ENUM']);
                }
            }
            foreach ($selectedProps as $selectedProp) {
                if (in_array($selectedProp, $stringProps)) {
                    if (!empty($ob['PROPERTY_' . $selectedProp . '_VALUE'])) {
                        $stringPropsValues[$selectedProp]['NAME'] = $propertiesInfo[$selectedProp]['NAME'];
                        $stringPropsValues[$selectedProp]['ID'] = $propertiesInfo[$selectedProp]['ID'];
                        $stringPropsValues[$selectedProp]['VALUES_ENUM'][] = $ob['PROPERTY_' . $selectedProp . '_VALUE'];
                        $stringPropsValues[$selectedProp]['VALUES_ENUM'] = array_unique($stringPropsValues[$selectedProp]['VALUES_ENUM']);
                    }
                }
                if (!empty($ob['PROPERTY_' . $selectedProp . '_VALUE'])) {
                    $item['PROPERTIES'][$selectedProp] = $ob['PROPERTY_' . $selectedProp . '_VALUE'];
                }
            }
            $items[] = $item;
            print_r(htmlspecialchars($item['TITLE'], ENT_XML1));
        }

//		die('end');

        foreach ($stringPropsValues as $propertyCode => $property) {
            foreach ($property['VALUES_ENUM'] as $id => $value) {
                $stringPropsValues[$propertyCode]['VALUES'][Cutil::translit($value, "ru",
                    array("replace_space" => "_", "replace_other" => "_"))] = $id;
            }
        }
        foreach ($items as $key => $item) {
            if (isset($stringPropsValues[$mainProperties['brand']['CODE']])) {
                $items[$key]['BRAND'] = $stringPropsValues[$mainProperties['brand']['CODE']]['VALUES'][Cutil::translit($item['BRAND'],
                    "ru", array("replace_space" => "_", "replace_other" => "_"))];
            }
            foreach ($item['PROPERTIES'] as $code => $value) {
                if (isset($stringPropsValues[$code])) {
                    $items[$key]['PROPERTIES'][$code] = Array(
                        'PROPERTY_ID' => $stringPropsValues[$code]['ID'],
                        'PROPERTY_VALUE_ID' => $stringPropsValues[$code]['VALUES'][Cutil::translit($value, "ru",
                            array("replace_space" => "_", "replace_other" => "_"))]
                    );
                } elseif (isset($listPropsValues[$code])) {
                    $items[$key]['PROPERTIES'][$code] = Array(
                        'PROPERTY_ID' => $listPropsValues[$code]['ID'],
                        'PROPERTY_VALUE_ID' => $value,
                    );
                }
            }
        }

        $xml .= '	<brands>' . PHP_EOL;
        if (isset($stringPropsValues[$mainProperties['brand']['CODE']])) {
            foreach ($stringPropsValues[$mainProperties['brand']['CODE']]['VALUES_ENUM'] as $id => $value) {
                $xml .= '		<brand id="' . $id . '">' . htmlspecialchars($value, ENT_XML1) . '</brand>' . PHP_EOL;
            }
        } elseif (isset($listPropsValues[$mainProperties['brand']['CODE']])) {
            foreach ($listPropsValues[$mainProperties['brand']['CODE']]['VALUES'] as $prop) {
                $xml .= '		<brand id="' . $prop['ID'] . '">' . htmlspecialchars($prop['VALUE'],
                        ENT_XML1) . '</brand>' . PHP_EOL;
            }
        }
        $xml .= '	</brands>' . PHP_EOL;
        $xml .= '	<attributes>' . PHP_EOL;

        foreach ($selectedProps as $selectedProp) {
            if (isset($stringPropsValues[$selectedProp])) {
                $xml .= '		<attribute id="' . $stringPropsValues[$selectedProp]['ID'] . '" name="' . $stringPropsValues[$selectedProp]['NAME'] . '" unit="">' . PHP_EOL;
                $xml .= '			<values>' . PHP_EOL;
                foreach ($stringPropsValues[$selectedProp]['VALUES_ENUM'] as $id => $value) {
                    $xml .= '				<value id="' . $id . '" attributeId="' . $stringPropsValues[$selectedProp]['ID'] . '">' . $value . '</value>' . PHP_EOL;
                }
                $xml .= '			</values>' . PHP_EOL;
                $xml .= '		</attribute>' . PHP_EOL;
            } elseif (isset($listPropsValues[$selectedProp])) {
                $xml .= '		<attribute id="' . $listPropsValues[$selectedProp]['ID'] . '" name="' . $listPropsValues[$selectedProp]['NAME'] . '" unit="">' . PHP_EOL;
                $xml .= '			<values>' . PHP_EOL;
                foreach ($listPropsValues[$selectedProp]['VALUES'] as $prop) {
                    $xml .= '				<value id="' . $prop['ID'] . '" attributeId="' . $listPropsValues[$selectedProp]['ID'] . '">' . $prop['VALUE'] . '</value>' . PHP_EOL;
                }
                $xml .= '			</values>' . PHP_EOL;
                $xml .= '		</attribute>' . PHP_EOL;
            }
        }

        $xml .= '	</attributes>' . PHP_EOL;
        $xml .= '	<products>' . PHP_EOL;

        foreach ($items as $item) {
            $xml .= '		<product id="' . $item['ID'] . '">' . PHP_EOL;
            $xml .= '			<url>' . $item['URL'] . '</url>' . PHP_EOL;
            $xml .= '			<categoryId>' . $item['SECTION'] . '</categoryId>' . PHP_EOL;
            $xml .= '			<name>' . htmlspecialchars($item['NAME'], ENT_XML1) . '</name>' . PHP_EOL;
            $xml .= '			<brandId>' . $item['BRAND'] . '</brandId>' . PHP_EOL;
            $xml .= '			<description><![CDATA[' . CMoxielabContentbox::strip((($item['DESCRIPTION']))) . ']]></description>' . PHP_EOL;
            $xml .= '			<sku>' . $item['SKU'] . '</sku>' . PHP_EOL;
            $xml .= '			<meta_title>' . $item['TITLE'] . '</meta_title>' . PHP_EOL;
            $xml .= '			<meta_keywords>' . $item['KEYWORDS'] . '</meta_keywords>' . PHP_EOL;
            $xml .= '           <meta_description>' . CMoxielabContentbox::strip((($item['DESCRIPTION']))) . '</meta_description>' . PHP_EOL;
            $xml .= '			<video/>' . PHP_EOL;
            $xml .= '			<images>' . PHP_EOL;
            foreach ($item['IMAGES'] as $image) {
                $xml .= '				<image id="' . $image['ID'] . '" title="">' . $image['LINK'] . '</image>' . PHP_EOL;
            }
            $xml .= '			</images>' . PHP_EOL;
            $xml .= '			<attributeValues>' . PHP_EOL;
            foreach ($item['PROPERTIES'] as $property) {
                $n = $property['PROPERTY_ID'] + $property['PROPERTY_VALUE_ID'];
                if ($n > 0) {
                    $xml .= '				<attributeValue attributeId="' . $property['PROPERTY_ID'] . '" valueId="' . $property['PROPERTY_VALUE_ID'] . '"/>' . PHP_EOL;

                }
            }
            $xml .= '			</attributeValues>' . PHP_EOL;
            $xml .= '		</product>' . PHP_EOL;
        }
        $xml .= '	</products>' . PHP_EOL;
        $xml .= '</catalog>';
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/upload/' . COption::GetOptionString($MODULE_ID, 'export_link',
                'contentbox.xml'), $xml);
        return "CMoxielabContentbox::GenerateXML();";
    }

    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        if ($GLOBALS['APPLICATION']->GetGroupRight("main") < "R") {
            return;
        }

        $MODULE_ID = basename(dirname(__FILE__));
        $aMenu = array(
            "parent_menu" => "global_menu_settings",
            "section" => $MODULE_ID,
            "sort" => 50,
            "text" => GetMessage("moxielab.contentbox_MODULE_NAME"),
            "title" => '',
            "icon" => "moxielab_contentbox_icon",
            "page_icon" => "",
            "items_id" => $MODULE_ID . "_items",
            "more_url" => array(),
            "items" => array()
        );

        $GLOBALS['APPLICATION']->AddHeadString('<style>.adm-submenu-item-link-icon.moxielab_contentbox_icon {background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAYAAAA8AXHiAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmZTk5MTU2NC1hMTBkLTQ5ZGYtYjQ3ZC02OWFkMGE0ZTE0MzEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QjhDODU1QUFBQTgxMTFFNkIyNUU5OUYxQzEyMTFERkQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QjhDODU1QTlBQTgxMTFFNkIyNUU5OUYxQzEyMTFERkQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ZTYyZjg0YWUtOGRhMC1lNTQyLTgzOGUtZWQ2ZmY0MmQyOWM0IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmZlOTkxNTY0LWExMGQtNDlkZi1iNDdkLTY5YWQwYTRlMTQzMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlMr+3QAABy7SURBVHja7J0JdFtXmYD/t+k97bItr4kdO3b2Jk3XdEu60rRhKG2hSYdDKeVQOGWAKWVm6AAD58CUMyxTTktbCoWW6UYpA9OdLpCmSUu6pNkcZ2t22/EmW7IlPent819JdmzHTmxLz5Lt+59zbVl6en7v3u/9/3//e+9/GcuygAqVXAtLq4AKBYsKBYsKBYsKFQoWFQoWFQoWFSoULCoULCoULCpUKFhUKFhUKFhUqFCwqFCwqMw84bM9wcGolvVF+AQW7mvqhV/u7YUzi0X4eLUL5vkEeOFYHBYGHNAS1yGsmGBYAAEHC1UuDnZHNGiVdYioJtxc54FdYRWOxjRYjMdzDANNERWuqHRCEr9ESpmTSx0vaybomfMUiyz37OG453MN3lk7epQFzx+TKxt8QtF8n1B7NK4FQ0nTH1ENb0K3RNW0eNMCjmXAcLCM7uQZJeDgokGJ7Z3jFkL7+7QjB/q08CdrXG14D/sePxBtXVvnjvUopkGukWcA3AK5dh46EwZIHJMq69sSqWs2wYLdeA9zPAIsLXbAM4di4MdrnIXHLw4I0CYbqXvlUBUU4fuz3Tzs7dXgOqwr/N/waouMdaLClxb44CuL/BDF+8xG5nqF/IJVyEKmMCJjIHAACAK4eZbtkPUqYODs3Z3qYhfPnnMsps//0Y5wOcJWigCyO7uSsLMjgbocv8hkTsCcOCfClYYVvxBGQA5HAD4w08ezPAsIp/lys9yFx3Wsb0vul3XzwzovvxsvZmupkz/u4hmTXA/DUI01JYFiUxoC1Ytq8WHFWro+kljVmTRW7uxRz8LPqxOKIaQORK2RooXJfInLlNNJP3QAA8eblgWyarJ43nI8V/nGFnkZHvfppgin4RHNZxYb20olbhNqoY1BiWnE69NZhoI1JUTARnZgg6NSmbe5I7nmo6h27cGofl5SMYtNAhCXIW8wPFwOW3cwKXz6dUJFiBmYu/l4Yi7LMp+SRLan3st/MN8rvIKX9IqTYw5oFgzRjBSsAlBP6LoA+j34mxHRH7usXTZu3hVRr5IVc3YKJjajXTINPekN2A8bn1aOsmIUNyaM1U0hdbVLZP9FMay/Vbi4ZwSG2YD3oaBFBYuClWcNhY2GzrWvWzE+vjOs3hJVzcsUzXSeBFOhSL8JZdOQxVSj+t32xOdFgV3ndbAbvAL7RELnX8L7ijIUrMlUUFaqbYiWQmacCcO87lf7+77SKRurFN1MNxo3RZqEyfxA84xay6nI+rXYu7t2a4+y0S+wDwYE7kXsACYYPMaiYNkIFTrHDpYFkWPZbsW8uimi3dWZ0C8zdEtIaaip7AlnNJlimNDca646zjMX9ij8mxLP/vwMjnndwTGmBVPHEZtSYKGpIPGsBR+G4ne/15lYqxqWa0ppqDEBltZgyJdwPKZdHUrql0RU81l09n/sEdi9JNRhUbBy50dJPCPujmi37Y+o3+qIarUpmNhpHAzKaDBVt1ybmuOf3+8VLl0QcPxkUUB4DOtDKfR1xmxhP7xMKirfoxgL3u1UntzUKj/YIeu1KQeLneYRxsE9SrxfvO+6jXj/m7EewlgfJCpfyDVQmBrLSmspws+WkLLupWPyPZ1xvX5ambyJAGYBu7Mz+WmEbDnHMN9xsMyzQvp9qrHGIo40QK6dPeoPH9vX9+iMh2qwecSucEdcb3h0X99jO8LKD0g9OQqwbgoOLDIwiw561U8bI//zTlviu7qJDjqFaqhgfZB6eft44j9+0hj5HakviSuspiwoU0jUumJYS586GH1gf7e6KhXcpEydyjTCm8fkm1rjevkcr/BVrL/GQrGKBYE56WE7SBfbgou2dytP7w9TqMZjGkl9bQ0lnzJM6wKRzLIogHrLm8ZKje+h+ib8uPBHTLMuC8nGI1hZDdT0jRMurK/uhLEUn9DfRXXrdifHbLKw16iZFmj4tOrWFATLtMZfD+QrB/p02NOrQkQx0acyLn/6UOwJsKxZMyaMYIdpNKwFj38UfQpr+LNBidtYjGaATJisdHED9T5pvGeb0W8vwjHe+ycwbmhPwp+OxGBbt3pJd1x/DD+imioXghqKY5i9tQHhtgtKxXc/Xu2GFaXiQL2PVbKdQZo3H8tFpuayzNkJ3XwE75pClcMeowGwsDtp/JpnmTNcearXrMFKzdwdRyHz0T3oYCUMq25bj/qgrJgLqZOe+1aNyMbSTe2JB+O6Ve3h2fQsonGUvINFZqqMtRhYFNTHh6Ka78kD0XtbI+oFVFPZ5T0zcKhHXfXb/X0/bY7rLuLAE1NojLHk3Xknq0fGKl7UVPvQJ7u3MfK9HaHk9cDT1Wf2wsXC+mZ53d0M7Lv7zKLvV6MTnzAmx4Wf1JbtU034e2fy9h0h5U5gWBqnmoxQBFbzX1vkf3urLfGPKjKlpqzG6UveNVal8/SnILEqsiDvF7sj5z26r++7eMMcXSo7iWEI3ZIeaur9XrHE7Ti72LG7VzPH8EyL+dVYJNB5uiKiH9WZ0ANvtiV+pKlmDY1VTb6/1S0bC5/cH/3+kZju4LEDRdaznqrk3RT2r+gdrRC/ivQG/3tX5BuNncpV1K/KH1wfdiTWbmhL3M6nVjWdWhnk3RRuCSmjfkbcRLIcfFdYXfl6i/y1gT5vVt1Qa/zh/uli0rJZcZQZ+vnTkfg3zg1Kb11aIe2K5kI12QWWcopGJhrrw27F9WBT77djilmU9VIshGqWjwesmJR5nQl8EZ4U7MltCSWhNapnN78fT9Yn6/UvHot9c1FAuI2s3Lark5g1WAv8o4f+yfRZ7I3cerRXvSZrE4hQLS+T4LFVpbC8WBzQiDOhY0dke3cSbtvQBtt7zPRq7okKgvm31sTN8/yOPy4pEl5JjDJCffUsV37BauwZeayQaJRQ0pyLqvefBhJsTFSstDf4tSW+AagAZla0YnmJBF+t4+CLrWEAbzHWiTGxE2FbaNhLfK1Z/jrPut5EbWhLaCtrsI7F9RHfJ9ld0K/6QkuftiRrE4g2T0TtV+3iYSZLtZMFMXQYUl6tvwTA0Cestfb3qqsX9zmuXzvX83uOgZybxKxbaiQHkORRaE8YS7d0KZ/NpVqZ8TsrcmgDkzJA60fYcvjaHZgYXEzatfioV/vyPJ/wUpHIRpUck5U1WCPleSLuVGOPcks4acyhY4G5hgubLJFIwzVr3sThwkbaE1Ev/Vlj+LqFAcdTw32tH59Xkl2nI9v7LJe4IWWOmycPxMLdYfUmSoEd3jyThktBzXX8AEA8kv57AlrLRC21PaTewgAjklm8xC/uL3nXWPU+YYiGJSGGdzqSn+4mC0tpMNS+rmIKLtRcbQcBKhtQc/nHr7nQmn4U1a7QDGv1pVXOF2I5jGtlDZZr0LIjQnpENcve60xSbWUnVKncICykEpIm++GqHz9cqP1M3RT+ju1V4eJeiOVwcnzWYL0fSg441mRC2Y5u5WOHovoy4Ki2so2swY4tceJVhKv9EMI1F5/0ccKFHa33QsnLlxQ7llS7+aZcOfFZg3V+qZTxBVOLKLlnD8ZutMisPoGCZZ/KGpyZkOSxYNNmsf0wQAWByzd2uLDd5KQ5y7LgmovLpCaSmbkgZpD23yYZ1Dwe15fu6VVX0dkLNjruQ+YPs4N8Li7t0BO45N703+MI5LzdnljTEtedZIguppv5B4vEsfqwEBX6dkfy8kjCCNoRYkgnNZ7ZwJL7Zwb0ySCo+l+wQkZzHUG4omOHCxXBoT7tnNa4vjyV+zcH1jBrsMh4YKnEkcWRDvSvVoMdiZsyA7ER1ZjRYIUTKiTJwgFmBKiAOaG5VPR7O8YBF4IVTRr+pl7t4goXD0Uil3+wyC4LoaQBzTF93uGodpYtZhDPaaFW/POROKiGOSOhIvf9f00t+ITp6d7gSFD1/01CEQSuzqPjgmsbOvHPHY0Lb7TK+Xfej8Q0IPkC0AxeEEqaZbb5V9jj/P3BKMixGFwTUIFnLDCZQusgDHaqmQH/5cRrAsAoeURHNPNMyvzplgmv7muH5wlYAn9qqPrPw2Y0V+cxgLIadOi96NCfWuMfjupLRBaqK5z8obyDlUo4x5GU2OYKME2wLSiacVafbzHg+W2t6EccxIrToWDmOPTDxIzk/7BDd7IYAIk5+ZjBn5PeHgknWJnJjaIwKJPhKaAauBZsXg3h6moGKEW4nB4Sbh+1IeOGNeeDbuWCggCLLEDtSBj+HWHtbPs3iMHKlfCSZ9enGsIRbgaPyOYXruEwjNTgwAxdCcowI2s3ZujxsaQGakLDLjefhmwsmooZ/j8zmitE4KrG+hsFLjxWUwyyOr3huhp3/k0h2a3KIet1Sd2cMykNSXws5Gv1+Yvg7gXLYDY6m2Yh5kFnRiJvrF9FE4gapKVXhv/asAde2318kDc8DqiG+1xdp4ELLc8+dOAfP9AnfrLGpeQVrMcPRInzfmafbgUnJX6FvcP6Ygc8cGEQGnwCTGdpKPHAbL8LrgnH4WBbBDWXMH6oBvtcGrISQjciOBvhco8IF/rMNcuDIjpkkBVYWTtEtzZ4iNao1lGNTopFQl9jWZEj62woU0XmFrthWUUA7xsmDtVAIDCjuULYCUjG07AN8zQcLBM8PyjW5j3c0BTRKrsVY/FkbsBH1sXNnGzcTGq4LGuo+puay2iu7taT4SIOPFqee3dFluUdrNda5MpdEXXuZK5stmDmzCZN3asFuYFqIGifgavn+FC48HNVM6HazQfyDtYNtW4/WqdiOm94kjoC2UKVypnBZOBKpuFSBsGFPuwZRY6SvIOV0KxqfKJKaOtPAl25gGq4z6WrCFd7egCbwIU9ww+6lCV5Dzc0hpUKheRin0Y+z8aD7fD+0a6UIWKYwVHzoeEDK2Ojzp8ThFVzy+2zhXZA1X8OApNOzGIbQHEleu9OaJWN0ryD1SIbRbppidNl9+wnPjgAX37mbUj0ZTYcZ4c33rAGxd6a0++EX910IdxyzlybzCAz7I0cQTXwOYFLAwij5gqUQ5/gyTpCmrUpbInrAd2aHvoqpmjw8IZdkAjHAFwCgCSkY0eiI11Sr/n0b4cjPcTiFPD4ODy8aQ/EVN3eC2RsgKr/HCSyT8xibydaxbgj72D1aoaL5ACYDmhZkMk30j+uxwzWUqcupAps2+qNsRmqAZ8rrbmMcGfWXGRtCmUdBGua9Ai9qIFuv3QJbGsPgxJNZMbnRmmM/r/RFIp+F9y+chF+n7eTrJEd+VxBNeBzEfOuQd7B0g2Ln06O+xdWzIM5RW54F533tKJgRvZ1mLSGIs/UBTWlcOW8ikkKO9gIFZMzQ5aTldDWdAthXTm/KlUKxj5PKlQM5KIjln1GP57RGLp2YnLjWHZDVQhguXlWoQu97GZqkjVVIYBVJnFRMtmPDunY7FdNmvmzsM/CWXkHq9rN9/As6NPP0ypUyOz2qRhwCJyWd7BqPXy3wDJJqrFs9uAZxn6oMpZH4gsArLODYrPIMr0ULJt9rCHz4m2CiohhwtIS1+68g/VORzKkW9BDd5qw3QbaDxURjoUuWdued7DaE0Z3UGLbaOPbZwUnDarUHtM87O1OMnkH65+X+JrPKRF3g0FtoW3KagAQO6EiQzlkNxEWVs8PNucdrA1tCblV1g/RXKOT4GcxNkKVGaNyCezhG+YG/p53sEhS1DoPf8ArcuqM3IpkMn0sO6FKp/MBneHiD+/qkvMO1j/UuOGicmejwEIz7RlOom3MNVQZfy7oFDaUSEJ3tleZ9SD0344nyAyAzhq3sL9HNuppw9tpCm2EKtUj5Mi05MMhLQZ5B6vey4OLZ9V9vdzm7Z1wLSXAhl7hEFBsgsoACLj53m+umPVu0JX1BNLsTWG3YkFn0oRat+MDl8hSP8sW98pmTZUJYXgk4ZBX5Pc4+QIYhF5ZIcEqLFfPdn4QlLh9YFIWbKFryGqh3ENFpMItvbk7rITfaI7mH6yIYkCHrIPIQvc5JY5NdDDazk6hTVBhmwkO3nA7hHdDsgZRxcg/WKmEq6n8AizZ+uwNTmANag5t1lo51lTEj/NKwh63yG3Qse3MHCiHrMEKoKoixS0wcHGFtLHeJ+ykYNnUK7QDqsyMhkqPtN4n8l0cy4DAFcCc9/6NMMl1ixzTU+cVXtrfo55FSchhr3BcC1bHCRUqAbckJFbXFb802yuCkqPkwVmjSRLbkkL2KJSwfKzK+bLPyYXp2KENfhaTY6iIIFhVPufmhmLpnWInB0EXnyp511gNvhOnIHmrZnu4988ucby+oVleR8cPCxwqImj2TJb7/eN7emSirfo/+eKikvyCdTCqDdHaXoG1LiiTnny7I3mDblgOuv1JjuJY1qkWrE4QKtRWRW5pBzruL8Q1I6cjclmDRYKjg6VHMSEoca8v9Dv+uiukrKETAHOptXIIVQosgOvnBf/wmUXBzmiOd/3IutlVpH5wSW2JzoC6okx6hOMZnQ5MFyBUTHp38Qq/c9+qGv8zDh579Q5uSMk7WG70o4YXknZ2WbHj5aVFjlfscOJZBoCZQUyxo4I0EahO9DTXNJT8bkFAOpzUzIGkJrlKbpK1KSweZUMfN89qa2pcDzVF1Ks0w3LlzNfC07TGdSDbzHpmwJ6IMTRRrTH11NpnPFARTHUTygPO7WvnB58okfjUrNFcS9ZgzfOPnBabdAjrfJ7XmsLqU88fiN0OjhyBRfac7kzC/Y098K9n+EBwONIPoDXc4YWTF9GO9D5zqvjRKJ9P5DPrNMcP+w55qaFrcd+WZninpZfsK5MbqMg/xDos9zof2Nwht24NJcgGpidd1llBKbvn38pS76FGGvUzH2qUN47LS+7cHHo+qpj1OQs/kA1csYLOdUahRo+AAezpK3ykvwf3uk5qXGb0Ywb/j+G9t5POMbyRR7uOoeciq8uP9SmwpbUvBViq7rKFivxGbbW43Pfa6vrSG2XNkHVr5Kx5j1xenV+NdTiqn9I/qHYLTTfUeu57fG/f/aM+tRPwDDX8sbnPCZuPt6VzlpNKZYftZTNk+OM0QyKnjREN3wBpop8zJ4Nw0v43g84hcLmDigDKsF0LSr331PsFWdY4sEuyBqtPPfUQgISVchH2ELeGlCt2hZTrgc+V242V5ER1PXch/nak4SIZ6ZhRdt4aYg4gh1CNdE525M2bsgI3S6gyyfFX1RY/sqBI2tSCfpudE1H47Jv31FcnY6+wysUlr691/+BIVDszppp1uTOJRhqmikxSWZL5l7EGaSf2NADkcMeHSVn3N0GoiGgGLKzwbfrhRdX3ljp5iGv2TpzLGqzKMYwrkXu7drZrG2q3e+5vjPwKVTKXs17iYLhIxZKE+Kmd3TkK1UA6S7JJjtB2VqX/20En3022UHHb3KPOGqyxXh8xieeXir9dUiKe0RRS7jwprWbWcOGFlNemK7i7LR2MYRgKVSpjLwOfWlT6s2vm+N/ejr1ABk6fdWpRkZjfACnpvYylkM3CyY5d95xb/J/nVEiv5TxwSnZ3JXCVzQEoqcSaMzPVN9OhAqgqdj98Rpnn/oDIgYHai4QXjNOUvGus8Sgd4reTtEeXVTq/2ditzFZ1a0lOZ0AMhotIuCNdu+PZ7na6QEUEH95lVb71/76i+vtlTkEn24VOVlA56//CMGMvZIA+iTdbLLJNy4PiHWjnj9qquYoq0o/swPZZMwgqsmOGS9yzqrrozrk+sdOFTzWZHcqPseRdYzknsLn4lVUuuKrKtWn98cRdP9za8xtZNYtyF4bIwEUagezeTk4b7hz2GE1nqNIDzAjWMVEQ7tAtaIyqJvrCzKTOveTzofK8CKOIIH2ixvXnHsVw/WJX5MGkbvlyahatDFxk93ZylZF2fI89sa3adIVKMyDoETtvXFT+5Qq38FZDQAK/yKaq1rSYqQPWRB4CMoxg4qNEBpIvr3Q+Gdct4dE9vfchXN6caq4BuGaf0FxjzdgyJaEywSUJ3d+6sOaOzy8Ovqrj7Sfxh2pO/mJPHvIopPNB4FpZLj0msoz6mz29v4hqaBbt0Fxkg20ika5BwEwvR93nFbu+s6L6S5+oK3quO5nffMN5n3dCniWECVbPdj71rbOKviA52PacOwOD4SoqO7H373SBKj2/6uhnFpV99qZ5xc/1qnrazYQZDFa/5iJxrhWl0nN1XmGdwLPpDIGWDXCVzAIIlKVbo7+3OJXjVFhPRS5pF+d0fA57fq+nF5xC3mdCFgRY/ZHgOPoDpmVtXF7iuHlZqfRGuvIsG+CqAvCXjjJfawpBhXJmpf+Nc6t8N2Onb6NqWAWzVrjgpmCSsVGvwDbe2uBZOy8gPIAVqOc00chguAKlJ+biTlYe9VxAlR5UMOqKPb+8ZUn5Oq+Db9KMwtozsuDASs2cTM1rYyJVTu6u1TXurxRJbGtO/a5+uMgeyP7g0O5tIUNlpZ88nyS0rawt+XqZW7wTO9Hh1ETAAlsEUJCTxvun5SJL2tKA45HzSqXragOZhRmWDXD5gjDEoy80qDKOKHlVV+pdv7jMc/3CEtdDhmWppJ4KcWFJwa5G6H9AY7pFpjhvXRwQblpaKt5VLKL20nME2BC4Ska+iHxDlXLQTeB5rvPS2pLvfXFZxQ0ix70va8YQBVtowkOBC5MJqKJjKtd7hZ/Xevi/tsnG3du7lRt1w5KGpKnOBq7UuCKeKNozdEl7XqDK7KZmmsDynNYQ9L4q8PwPav3SFpFjUXEX/mLNKbF+isn8JLMeAw62cU2169aVFc61QRf/Jkt2HjOyXAw3AFd5WnP178mbD6iIYO9YwPdKfNL784KeW8+vCqz1O7gtREvpUyRFFA9TSdKjFkR76UUi+2KDT1jfnWQ/GdfNO0JJ8xKVjGFMVIP1w5WKcREbHB4URZ0kqDIBqMoi164raop+7Ra4p99tj3XrqLlSq2mm0CrdqQUWDOkcEZMQFznm6XVzfS9vCymf2Nyl3BLTjJWqZjkH5mAx44WLScNFfsd67IUK0gHO9JwxVqvwu7ZfOMv/RF1A+mO1V2w/HEmmNNRUzL7Jw1QWC1IV7+KZ3iKRe3JJQPhfv0O6vCNprNsdUa9MKOZskzQcacuxzjHqj2mRACqReARO7BeYJVRwIudnWhky4HUJvRUe6U3Ngj/U+52vXjLLH+mQVZhKZm/6gZVpW2IBSUISNBfJ2W7uL/Ve/i8iC/NKJW7NgT5tzcGofl5SNYpSg/xcRuWdCrQhcOHvePgEROOBatAD0K8RU/laBS5e5hF3lTgdr108y/8ivrltU2uvQUIHsm6knPOpnpti6oM12ERaBDBIJWflGPjowjLpvsUB4aHupLm0NWGs7EoaK3f2qGfhYdUJxRBSDZ6eqDSwl8zJcAXTn8V7R/a5RnK+zcx0aGJaTYZkJCZItdcGnHsDkvCW18FvCkj8+yLPxrwiBzG8aBI5n06JeXiYpkIsYHrsETSPg916RcCxtU02Hpjl4iqxzc8+HNUXu3j2nA9DynySykC3oDRpWKypmyfAYKz0NOdUABX/TkSGQtXv/PT3SgWexJtILtZIwrA6agOeDnz7HZco7HewzHsVHvFQjU9UjvUqqa8p+E8JUCZY0y57zrQFa0B5QDqKT3Ypk3XT8AlsC7ZlC2qzF0pElkMt5rm1wTtrR4+y4PljcmVDkVQ03yfUHo1rQexp+iOq4U0YjKj6g7xJpqAmYilzxrKs6eA5zScJ8VkeMVzlFvZsbOltKXY7E5dV+5pePNC1d26xRxZZS26JaWS7NgTJBNJzJT08B6I0nVMxZZ0UhAqVkYQmcqRCwaJCwaJCwaJChYJFhYJFhYJFhQoFiwoFiwoFiwoVChYVChYVChYVKhQsKhQsKhQsKlRyI/8vwADZU3bzUXDiWgAAAABJRU5ErkJggg=="); background-position: 50% center; background-size: 24px auto;}</style>');

        if (file_exists($path = dirname(__FILE__) . '/admin')) {
            if ($dir = opendir($path)) {
                $arFiles = array();
                while (false !== $item = readdir($dir)) {
                    if (in_array($item, array('.', '..', 'menu.php'))) {
                        continue;
                    }

                    if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $MODULE_ID . '_' . $item)) {
                        file_put_contents($file,
                            '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $MODULE_ID . '/admin/' . $item . '");?' . '>');
                    }

                    $arFiles[] = $item;
                }

                sort($arFiles);

                foreach ($arFiles as $item) {
                    $aMenu['items'][] = array(
                        'text' => GetMessage("moxielab.contentbox_" . $item),
                        'url' => $MODULE_ID . '_' . $item,
                        'module_id' => $MODULE_ID,
                        "title" => "",
                    );
                }
            }
        }
        $aModuleMenu[] = $aMenu;
    }
}

?>
