<?
$MESS["TITLE"] = "Выгрузка товаров";
$MESS["MAIN_TAB_SET"] = "Общие настройки";
$MESS["PROPERTY_TAB_SET"] = "Свойства товаров";
$MESS["SELECT_IBLOCK_ID"] = "Инфоблок с товарами магазина";
$MESS["SELECT_IBLOCK"] = "Выберите инфоблок";
$MESS["IBLOCK_PROPERTIES"] = "Настройки инфоблока";
$MESS["EXPORT_PROPERTIES"] = "Настройки выгрузки";
$MESS["EXPORT_PERIOD"] = "Выгружать с периодом";
$MESS["EXPORT_PERIOD_EXT"] = "минут (0 или пусто для ручной проверки)";
$MESS["EXPORT_LINK"] = "Файл выгрузки";
$MESS["APPLY_EXPORT"] = "Сохранить и выгрузить в XML";
$MESS["BRAND_PROPERTY"] = "Свойство товара содержащее производителя";
$MESS["TITLE_PROPERTY"] = "Свойство товара содержащее Meta Title";
$MESS["DESCRIPTION_PROPERTY"] = "Свойство товара содержащее Meta Description";
$MESS["KEYWORDS_PROPERTY"] = "Свойство товара содержащее Meta Keywords";
$MESS["MORE_PHOTOS_PROPERTY"] = "Свойство товара дополнительные фотографии";
$MESS["VIDEO_PROPERTY"] = "Свойство, содержащее ссылку на видео";
$MESS["PROPERTY_SELECT"] = "Выберите свойство";
$MESS["EXPORT_SUCCESS"] = "Выгрузка успешно завершена.";
$MESS["EXPORT_SUCCESS_EXT"] = "Скопируйте эту ссылку в поле \"URL файла каталога\" в настройках магазина.";
?>